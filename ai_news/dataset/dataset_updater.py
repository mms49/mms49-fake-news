import os
from services.mongodb import mongo_db
from ai_news.gpt2_keyword_generation.get_keywords import get_keywords
MY_DIR = os.path.dirname(__file__)  # get relative parent directory location
TITLES_DATASET_PATH = os.path.join(MY_DIR, 'titles.txt')
ARTICLES_DATASET_PATH = os.path.join(MY_DIR, 'articles.txt')

START_OF_TEXT = '<|startoftext|>'
END_OF_TEXT = '<|endoftext|>'


def update_datasets(a, keywords):
    """
    Update the training dataset with the data from given parsed article a
    :param a: Dictionary of parsed article's properties
    :param keywords: list of string - keywords extracted from the article
    :return:
    """
    #   get title of parsed article
    title = a["title"]
    #   get the article's body
    article = a["article"]
    #   format the title and the keywords to add to the keywords->titles dataset
    title_generator_input = format_title_from_keywords(title, keywords)

    #   get previous keywords->titles dataset
    with open(TITLES_DATASET_PATH, 'r') as f:
        title_training_data = f.read()
    #   add new data to it
    title_training_data += title_generator_input
    #   write the new dataset back to the file
    with open(TITLES_DATASET_PATH, 'w') as f:
        f.write(title_training_data)

    #   format the article and the title to add to the titles->articles dataset
    article_generator_input = format_article_from_title(article, title)
    #   get previous titles->articles dataset
    with open(ARTICLES_DATASET_PATH, 'r') as f:
        article_training_data = f.read()
    #   add new data to the dataset
    article_training_data += article_generator_input
    # write the new dataset back to the file
    with open(ARTICLES_DATASET_PATH, 'w') as f:
        f.write(article_training_data)


def create_datasets_from_scratch(initialize=True):
    """
    Create dataset from scratch.
    :param initialize: whether to clear the dataset or add on top of existing one
    :return:
    """
    if initialize:
        with open(TITLES_DATASET_PATH, 'w') as f:
            f.write('')
        with open(ARTICLES_DATASET_PATH, 'w') as f:
            f.write('')
    MAX_ARTICLES_SIZE = 10000
    BACKUP_CYCLE = 100

    get_article = mongo_db.get_parsed_articles(limit=10000)
    i = 0
    titles_dataset = []
    articles_dataset = []
    finished = False
    article = None
    while not finished:
        try:
            article = next(get_article)
        except StopIteration:
            finished = True
        title = article["title"]
        body = format_article_body(article["paragraphs"])
        keywords = get_keywords([title])

        titles_dataset.append(format_title_from_keywords(title, keywords))
        articles_dataset.append(format_article_from_title(body, title))

        # every several checks, save results to file and clear buffers (just incase becomes unbearably large)
        if (i > 0 and i % BACKUP_CYCLE == 0) or finished:
            with open(TITLES_DATASET_PATH, 'a', encoding='utf-8') as f:   # push to keywords->titles dataset
                f.write('\n'.join(titles_dataset))
            with open(ARTICLES_DATASET_PATH, 'a', encoding='utf-8') as f: # push to titles->articles dataset
                f.write('\n'.join(articles_dataset))
            # clear buffers
            titles_dataset = []
            articles_dataset = []

        i += 1


def format_article_body(paragraphs):
    return '\n'.join(paragraphs)


def format_title_from_keywords(title, keywords):
    """
    Create the text to train the network of keywords to titles
    :param title: str - a title
    :param keywords: list of str - list of the keywords relevant to the title
    :return: str - a formatted representation of the correlation between the keywords and the title
    """

    # <|startoftext|>~^keyword1,keyword2,keyword3~@Hello world this is a title!<|endoftext|>
    result = START_OF_TEXT + "~^" + ','.join(keywords)+"~@"+title+END_OF_TEXT
    return result


def format_article_from_title(article, title):
    """
    Create the text to train the network of titles to articles
    :param article: str - an article
    :param title: str - a title
    :return: str - a formatted representation of the correlation between the keywords and the title
    """
    # <|startoftext|>~&Hello world this is a title!~*It has been recently discovered that pep8 is annoying.<|endoftext|>
    result = START_OF_TEXT + "~&" + title + "~*" + article + END_OF_TEXT
    return result


#   create_datasets_from_scratch(True)
