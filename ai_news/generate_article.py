import time
import gpt_2_simple as gpt2
import os
from random import choices, randint
client_on = False
try:
    from services.mongodb import mongo_db
    from services.mongodb.collections import Author
    client_on = True
except Exception as e:
    print(e)

sess = None
MODEL_NAME = "345M"
MY_DIR = os.path.dirname(__file__)  # get relative parent directory location
MODELS_DIR = os.path.join(MY_DIR, 'models') # get models directory
TITLE_GENERATOR_NAME = 'title_generator'    # name of title generator checkpoint dir
ARTICLE_GENERATOR_NAME = 'article_generator'    # name of article generator checkpoint dir
TITLE_GENERATOR_CHECKPOINT_PATH = os.path.join(MODELS_DIR, TITLE_GENERATOR_NAME)
ARTICLE_GENERATOR_CHECKPOINT_PATH = os.path.join(MODELS_DIR, ARTICLE_GENERATOR_NAME)

ARTICLES_DATASET = os.path.join(MY_DIR, 'dataset/articles.txt')
TITLES_DATASET = os.path.join(MY_DIR, 'dataset/titles.txt')

START_OF_TEXT = "<|startoftext|>"
KEYWORD_SEP = ','
TITLE_START = '~@'

"""
This file is responsible for generating articles based on recent trending keywords
"""


#   start_up_model()  # load the models (run at start of process)

def start_up_process():
    """
    Initialize the tensorflow session to use with the generators
    :return: void
    """
    global sess
    sess = gpt2.start_tf_sess()


def train_title_generator(train_data_path=TITLES_DATASET, epochs=10):
    """
    Fine tune the model responsible for creating titles from formated keywords
    :param train_data_path: str - path of the train data file
    :param epochs:  int - number of iterations to train on batch
    :return:
    """
    fine_tune(train_data_path, TITLE_GENERATOR_CHECKPOINT_PATH, epochs=epochs)


def train_article_generator(train_data_path=ARTICLES_DATASET, epochs=10):
    """
    Fine tune the model responsible for creating articles from title.
    :param train_data_path: str - path of the train data file
    :param epochs: int - number of iterations to train on dataset
    :return:
    """
    fine_tune(train_data_path, ARTICLE_GENERATOR_CHECKPOINT_PATH, epochs=epochs)


def load_model(parent_path, name):
    """
    Download the model if doesn't exist (not used currently)
    :param parent_path:
    :param name:
    :return:
    """
    if not os.path.isdir(os.path.join(parent_path, name)):
        print(f"Downloading {MODEL_NAME} model...")
        gpt2.download_gpt2(model_name=MODEL_NAME)  # model is saved into current directory under /models/124M/


def fine_tune(material_path, checkpoint_dir='checkpoint', epochs=10):
    global sess
    """
    Fine tune a model on a given dataset file
    :param material_path: dataset path
    :param checkpoint_dir: where to save the upgraded model
    :param epochs: int - number of iterations to train on dataset
    :return:
    """
    gpt2.finetune(sess, material_path, model_name=MODEL_NAME, model_dir=MODELS_DIR, steps=epochs,
                  checkpoint_dir=checkpoint_dir, sample_num=0, overwrite=True, restore_from='fresh')
    sess = gpt2.reset_session(sess)



def run_model(text, max_length, model_dir=None, model_name=None, model_checkpoint_dir=None, temperature=1.0, top_k=40,
              nsamples=1, batch_size=1):
    """
    Generate text from the given [text] field and return a list of all results
    :param text: str - input text
    :param max_length: int - maximum length of output text
    :param model_checkpoint_dir:   str - path of checkpoint to take from
    :param temperature: float - temperature of generator: how free is the generator with decisions(lower, more conservative, higher, more radical)
    :param top_k:   int - number of top results to pick randomly from
    :param nsamples:    int - how many samples to generate
    :param batch_size:  int - how many samples to generate in one batch (not using currently so leave at 1)
    :return: list - list of all generated results
    """
    global sess
    if model_dir is None and model_name is None:
        gpt2.load_gpt2(sess, checkpoint_dir=model_checkpoint_dir)  # , model_dir=MODELS_DIR, model_name=MODEL_NAME)
    else:
        gpt2.load_gpt2(sess, model_dir=MODELS_DIR, model_name=MODEL_NAME)
    print("given text: " + str(text))
    result = gpt2.generate(sess,
                           checkpoint_dir=model_checkpoint_dir,
                           return_as_list=True,
                           temperature=temperature,
                           top_k=top_k,
                           nsamples=nsamples,
                           batch_size=batch_size,
                           length=max_length,
                           prefix=text,
                           truncate="<|endoftext|>",
                           include_prefix=True,
                           sample_delim=''
                           )

    for i in range(len(result)):  # remove initial text from result
        result[i] = result[i][len(text):]

    print("result: " + ('\n' + ('*' * 20) + '\n').join(result))
    #   print("len(result): " + str(len(result)))
    sess = gpt2.reset_session(sess)
    return result


def format_keywords(words):
    """
    Concatenate several keywords in the conventional format known to the model
    :param words: list of str objects (each one is a word)
    :return: str format of the joined words
    """
    return START_OF_TEXT + '~^' + ','.join(words) + TITLE_START
    #   return START_OF_TEXT + KEYWORD_SEP.join(words) + TITLE_START


def format_title(title):
    """
    Reformat a title string to be fed to the generator
    :param title: str - title to reformat
    :return:    str - reformatted title
    """
    assert len(title) > 0
    """if title[-1] not in '.:-!?':
        title += '.'"""
    return START_OF_TEXT + '~&' + title + "~*"


def is_title_valid(title):
    bad_omens = ['startoftext', 'endoftext', '~@', '~^']
    for omen in bad_omens:
        if omen in title:
            return False
    return True


def pick_keywords(options):
    """
    Pick several trending keywords and put them in order in the right format
    :param options: dict - key is string of word, and value is it's frequency value
    """
    max_keywords = 5
    min_keywords = 1
    keywords = []
    number_of_keywords = randint(min_keywords, max_keywords + 1)

    keywords = list(set(choices(list(options.keys()), weights=options.values(), k=number_of_keywords)))

    return keywords


def generate_title(keywords_sequence, with_checkpoint=False):
    """
    Use a Fine Tuned GPT-2 Model to generate a title from sequence of keywords
    :param keywords_sequence: a str containing the keywords
    :return: a str of the generated title
    """
    maximum_length = 200
    temperature = 1.0
    checkpoint = TITLE_GENERATOR_CHECKPOINT_PATH
    model_name = MODEL_NAME
    model_dir = MODELS_DIR
    if with_checkpoint:
        model_name = None
        model_dir = None
    print("checkpoint: " + str(checkpoint))
    title = run_model(keywords_sequence, maximum_length, model_name=model_name, model_dir=model_dir,
                      model_checkpoint_dir=checkpoint,
                      temperature=temperature)[0]
    return title


def generate_article_from_title(title, maximum_length=2000, with_checkpoint=False):
    """
    Use the GPT-2 Model to generate an article from a given title
    :param title: str
    :param maximum_length: int - maximum length for the article
    :return: a str of the article
    """
    temperature = 1.0
    title = format_title(title)

    checkpoint = ARTICLE_GENERATOR_CHECKPOINT_PATH
    model_name = MODEL_NAME
    model_dir = MODELS_DIR
    if with_checkpoint:

        model_name = None
        model_dir = None
    article = \
    run_model(title, maximum_length, model_name=model_name, model_dir=model_dir, model_checkpoint_dir=checkpoint,
              temperature=temperature)[0]
    return article


def create_fake_news():
    """
    A main function.
    creates an AI generated fake article from random relevant keywords.
    Process:
    -Pick words randomly, with consideration of their relevance.
    -Reformat picked words to a sequence the model recognizes.
    -Use the Title generator model to generate a title from the keywords.
    -Use the article generator model to generate an article from the generated title.
    -Encapsulate in the correct format for the database
    :return: a dictionary object ready to be inserted to the db
    """
    assert client_on
    popular_keywords = mongo_db.get_popular_keywords(-1)
    title = ''
    keywords = []
    valid_title = False
    attempts = 0
    while not valid_title and attempts < 100:
        keywords = pick_keywords(popular_keywords)
        keywords_sequence = format_keywords(keywords)
        title = generate_title(keywords_sequence, with_checkpoint=True)
        print("title: \n" + title+'\n'+('*&*&*'*20)+'\n')
        valid_title = is_title_valid(title)
        attempts += 1
    article = generate_article_from_title(title, maximum_length=1000, with_checkpoint=True)
    print("article: \n"+article+'\n')
    epoch_of_creation = int(time.time())
    date_of_creation = str(time.strftime("%d/%m/%y %H:%M"))
    summary = ', '.join(keywords)
    author = get_author_by_name()
    db_item = {"title": title,
               "author": author,
               "date": date_of_creation,
               "summary": summary,
               "article": article,
               "time_epoch": epoch_of_creation,
               "comments": []}
    mongo_db.insert_article(db_item)
    return db_item


def get_author_by_name(name=None):
    kwargs = dict()
    if name:
        kwargs['name'] = name

    assert client_on
    authors = mongo_db._find_data(Author, **kwargs)
    return authors[0]["_id"]


start_up_process()
#   article = create_fake_news()
#   print("Title: " + str(article["title"]) + "\n\n" + "Body: " + str(article["article"]))

#   train_title_generator('dataset/titles.txt', epochs=30)
#   train_article_generator('dataset/articles.txt', epochs=30)
