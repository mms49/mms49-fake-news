from ai_news.generate_article import create_fake_news
import sys


def main():
    """
    Main function of script that creates articles.
    (designed to be called from commandline)
    :return:
    """
    arguments = sys.argv[1:]
    assert len(arguments) <= 1
    num_of_articles = 1
    if len(arguments) == 1:
        num_of_articles = int(arguments[0])
    for i in range(num_of_articles):
        create_fake_news()


if __name__ == '__main__':
    main()
