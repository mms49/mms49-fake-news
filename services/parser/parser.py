from abc import abstractmethod, ABC
from typing import List

from bs4 import BeautifulSoup, element
from services.mongodb import mongo_db
from services.mongodb.collections import (
    ParserJobs, Articles, RawScrapedData, ScrapedInfoWarsData, ScrapedBuzzFeedData, ParsedArticles
)
from services.mongodb.aps import AggregationPipeLineStages as Aps
from services.scraper import InfoWarsScraper, BuzzfeedScraper
from requests import head as get_head_of_response, get as get_request
import stop_words
import re

stop_words = stop_words.get_stop_words('en')


class Parser(ABC):
    def __init__(self, scraped_data_cls=RawScrapedData):
        self.scraped_data_cls = scraped_data_cls
        self.raw_article: element.Tag = None
        self.unparsed_articles = None

    def _get_article_cls(self):
        return self.scraped_data_cls

    def extract_all_parse_jobs_article_ids(self):
        pipes = list()
        pipes.append(Aps.match_pipe(article_cls=self._get_article_cls().get_collection_name(), parsed=False))
        pipes.append(Aps.project_pipe(['_id', 'article_id']))

        agg = mongo_db.aggregate_data(ParserJobs, pipes)
        return [d['article_id'] for d in agg]

    @classmethod
    def update_all_parsed_jobs(cls, parsed_article_ids):
        mongo_db.update_documents(ParserJobs, 'article_id', parsed_article_ids, parsed=True)

    def extract_all_raw_scraped_data(self, unparsed_article_ids):
        pipes = list()
        pipes.append(Aps.match_pipe(article_id={'$in': unparsed_article_ids}))
        pipes.append(Aps.project_pipe(['_id', 'article_id', 'raw_data']))
        return mongo_db.aggregate_data(self.scraped_data_cls, pipes)

    @abstractmethod
    def extract_raw_article_from_raw_data(self, raw_data):
        pass

    @abstractmethod
    def extract_keywords_from_article(self, raw_article):
        pass

    @abstractmethod
    def extract_title_from_article(self, raw_article):
        pass

    @abstractmethod
    def extract_all_quotes_from_article(self, raw_article):
        pass

    @abstractmethod
    def extract_all_media_assets_from_article(self, raw_article):
        pass

    @abstractmethod
    def extract_all_network_urls_from_article(self, raw_article):
        pass

    @abstractmethod
    def extract_all_paragraphs_from_article(self, raw_article):
        pass


class InfoWarsParser(Parser):
    def __init__(self, update_database=True):
        Parser.__init__(self, ScrapedInfoWarsData)
        self._paragraphs = list()
        self.update_db = update_database
        self.raw_data = None

    def extract_raw_article_from_raw_data(self, raw_data):
        self.raw_data = BeautifulSoup(raw_data, features='html.parser')
        return self.raw_data.find('article')

    @classmethod
    def process_to_only_text(cls, s: str):
        return str().join(c for c in s if c.isalpha()).lower()

    def extract_keywords_from_article(self, raw_article, limit=20):
        words_list = [self.process_to_only_text(w) for w in
                      ' '.join(p.text for p in self._paragraphs).split(' ') if w not in stop_words]
        word_count = dict()
        for w in words_list:
            if w not in word_count and bool(w):
                word_count[w] = 0
            if bool(w):
                word_count[w] += 1

        keywords = {w: c for w, c in sorted(word_count.items(), key=lambda x: x[1], reverse=True)}

        for w, c in keywords.items():
            mongo_db.update_count_of_keyword(w, c)

        return list(keywords.keys())[:limit]

    def extract_title_from_article(self, raw_article):
        if len(self._paragraphs):
            return self._paragraphs[0].text

        return str()

    def extract_all_quotes_from_article(self, raw_article):
        return [p.text for p in self._paragraphs if p.text.startswith('“') and '”' in p.text]

    def extract_all_media_assets_from_article(self, raw_article):
        urls = self.extract_all_network_urls_from_article(raw_article)
        mime_types = ['image', 'video', 'audio']
        assets = list()
        for url in urls:
            head = get_head_of_response(url)
            if 'Content-Type' in head.headers and any(t in head.headers['Content-Type'] for t in mime_types):

                assets.append(url)

        return assets

    def extract_all_network_urls_from_article(self, raw_article):
        re_str = r"(?P<url>https?://[^\s]+)"
        return [re.search(re_str, p.text).group("url") for p in self._paragraphs if 'https://' in p.text]

    def extract_all_paragraphs_from_article(self, raw_article):
        return [p.text for p in self._paragraphs][1:]

    def parse_articles(self):
        unparsed_article_ids = self.extract_all_parse_jobs_article_ids()
        self.update_all_parsed_jobs(unparsed_article_ids)
        self.unparsed_articles = self.extract_all_raw_scraped_data(unparsed_article_ids)
        raw_parsed_articles = list()
        for unparsed_article in self.unparsed_articles:
            json = dict()
            raw_article = self.extract_raw_article_from_raw_data(unparsed_article['raw_data'])
            if not raw_article:
                continue
            self._extract_paragraphs(raw_article)
            json['article_id'] = unparsed_article['article_id']
            json['title'] = self.extract_title_from_article(raw_article)
            json['paragraphs'] = self.extract_all_paragraphs_from_article(raw_article)
            json['keywords'] = self.extract_keywords_from_article(raw_article)
            json['quotes'] = self.extract_all_quotes_from_article(raw_article)
            json['media_urls'] = self.extract_all_media_assets_from_article(raw_article)
            json['network_urls'] = self.extract_all_network_urls_from_article(raw_article)

            raw_parsed_articles.append(json)

        if self.update_db:
            self.update_all_parsed_jobs(unparsed_article_ids)

        return [ParsedArticles(**j) for j in raw_parsed_articles]

    def _extract_paragraphs(self, raw_article):
        self._paragraphs: element.ResultSet = raw_article.findAll('p')


class BuzzFeedParser(Parser):
    def __init__(self, update_database=True):
        Parser.__init__(self, ScrapedBuzzFeedData)
        self.update_db = update_database

    def extract_raw_article_from_raw_data(self, raw_data):
        soup = BeautifulSoup(raw_data, features='html.parser')
        raw_article = soup.find('div', {'id': 'js-post-container'})
        return raw_article

    def extract_keywords_from_article(self, raw_article):
        pass

    def extract_title_from_article(self, raw_article):
        div_title_class = 'buzz-title xs-mb1'

        pass

    def extract_all_quotes_from_article(self, raw_article):
        pass

    def extract_all_media_assets_from_article(self, raw_article):
        pass

    def extract_all_network_urls_from_article(self, raw_article):
        pass

    def extract_all_paragraphs_from_article(self, raw_article):
        pass


class InfoWarsApiParser(InfoWarsParser):
    wp_json_api_path = '/'.join(('wp-json', 'wp', 'v2'))

    def __init__(self, per_page=100, content_type='posts'):
        Parser.__init__(self)
        self.per_page = '?per_page=%s' % per_page
        self.content_type = content_type
        self._paragraphs = list()

    def extract_all_raw_scraped_data(self, unparsed_article_ids):
        iw_api_response = get_request(
            'https://www.%s' % '/'.join([
                InfoWarsScraper.get_website_url(),
                self.wp_json_api_path,
                self.content_type,
                self.per_page
            ]))
        raw_content = iw_api_response.json()

        return raw_content

    def extract_raw_article_from_raw_data(self, raw_data):
        return raw_data

    def extract_title_from_article(self, raw_article):
        raw_title = raw_article.get('title', dict()).get('rendered', str())
        title = BeautifulSoup(raw_title, features='html.parser')
        return title.text

    def extract_all_paragraphs_from_article(self, raw_article):
        raw_content = raw_article.get('content', dict()).get('rendered', str())
        content = BeautifulSoup(raw_content, features='html.parser')
        self._paragraphs = content.findAll('p')

        return [p.text for p in self._paragraphs]

    def parse_articles(self):
        raw_parsed_articles = list()
        raw_articles: List[dict] = self.extract_all_raw_scraped_data([])
        for raw_article in raw_articles:
            json = dict()
            json['article_id'] = raw_article['id']
            json['title'] = self.extract_title_from_article(raw_article)
            json['paragraphs'] = self.extract_all_paragraphs_from_article(raw_article)
            json['keywords'] = self.extract_keywords_from_article(raw_article)
            json['quotes'] = self.extract_all_quotes_from_article(raw_article)
            json['media_urls'] = self.extract_all_media_assets_from_article(raw_article)
            json['network_urls'] = self.extract_all_network_urls_from_article(raw_article)

            raw_parsed_articles.append(json)

        return [ParsedArticles(**j) for j in raw_parsed_articles]
