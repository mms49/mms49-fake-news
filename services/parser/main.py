import sys
from services.parser import InfoWarsParser, InfoWarsApiParser
from services.mongodb import mongo_db
from services.mongodb.collections import ParsedArticles

if __name__ == '__main__':
    main_type = None
    if len(sys.argv) > 1:
        main_type = sys.argv[1]

    else:
        main_type = 'IW'

    if main_type == 'IW':
        s = InfoWarsApiParser(per_page=50)
        parsed_if_articles = s.parse_articles()
        mongo_db.insert_bulk(ParsedArticles, [p.to_json() for p in parsed_if_articles])

    elif main_type == 'BF':
        pass
    else:
        raise Exception('No supported main Type')

    pass
