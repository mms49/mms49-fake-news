
class AggregationPipeLineStages:
    def __init__(self):
        pass

    @classmethod
    def match_pipe(cls, **kwargs):
        return {'$match': kwargs}

    @staticmethod
    def unit_dict_to_tuple(d: dict) -> tuple:
        return list(d.items())[0]

    @staticmethod
    def _create_categories_pipeline(data_object, operation) -> tuple:
        if isinstance(data_object, str):
            return data_object, operation

        return AggregationPipeLineStages.unit_dict_to_tuple(
            {fieldname:
             dict([AggregationPipeLineStages._create_categories_pipeline(category, operation)
                   for category in categories]) for fieldname, categories in data_object.items()
             }
        )

    @staticmethod
    def project_pipe(categories, include=True) -> dict:
        """
        this is the document why of JOIN in SQL
        :param categories: categories dictionary
        :param include: to include the said categories or exclude them (IE 1 / -1)
        :return: a pipeline for include/exclude categories ex: {'category': {'sub_category_1', 'sub_category_2'}} -> \
        {'category': {{'sub_catgory_1': operation}, {'sub_category_2': operation}}
        """
        if not categories:
            return {}

        operation = 1 if include else -1

        return {
            '$project':
                dict([AggregationPipeLineStages._create_categories_pipeline(field, operation)
                      for field in categories])
                }

    @staticmethod
    def lookup_pipe(collection_name_to_join, field_input_doc, field_other_doc, new_array_field_name):
        """
        this is the document why of JOIN in SQL
        :param collection_name_to_join:
        :param field_input_doc:
        :param field_other_doc:
        :param new_array_field_name:
        :return: the documents from the query with an array of all the matching documents in the new_array_field_name
        """
        return {
            "$lookup":
            {
                "from": collection_name_to_join,
                "localField": field_input_doc,
                "foreignField": field_other_doc,
                "as": new_array_field_name
            }
        }

    @staticmethod
    def unwind_pipe(path, preserve_null_and_empty_arrays=True):
        return {
            "$unwind": {
                "path": path,
                "preserveNullAndEmptyArrays": preserve_null_and_empty_arrays
            }
        }

    @staticmethod
    def facet_pipe(facet_pipeline, unwind_path='$metadata'):

        return [{
            '$facet': {
                'metadata': [AggregationPipeLineStages.count_pipe('total')],
                'data': [{'$addFields': {'_id': '$_id'}}] + (facet_pipeline or list())
            }
        }, AggregationPipeLineStages.unwind_pipe(unwind_path)]

    @staticmethod
    def pagination_pipeline(page=-1, limit=-1):
        pipeline = list()
        if limit != -1:
            pipeline.append({'$skip': (page - 1) * limit})
            pipeline.append({'$limit': limit})

        return pipeline

    @staticmethod
    def count_pipe(field):
        return {'$count': field}

    @staticmethod
    def sort_pipe(**kwargs):
        return {'$sort': kwargs}
