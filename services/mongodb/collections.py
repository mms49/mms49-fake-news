import abc
from bson.objectid import ObjectId
from datetime import datetime


class Collection(abc.ABC):
    def __init__(self):
        self.meta = dict(meta_name=self.get_collection_name())

    @classmethod
    @abc.abstractmethod
    def get_collection_name(cls):
        pass

    def to_json(self):
        return self.__dict__


class General(Collection):
    def __init__(self, **kwargs):
        Collection.__init__(self)
        self.type = kwargs.get('id_counter')
        self.count = kwargs.get('count')

    @classmethod
    def get_collection_name(cls):
        return 'General'


class Articles(Collection):
    def __init__(self, title, author, date, summary, article, time_epoch, comments,
                 comment_ids=None, author_id=0):
        Collection.__init__(self)
        self.title: str = title
        self.author: str = author
        self.date: datetime = date
        self.summary = summary
        self.article: str = article
        self.time_epoch: int = time_epoch
        self.comments: list = comments

    @classmethod
    def get_collection_name(cls):
        return 'Articles'

    def add_comments_ids_to_article(self, comment_ids: list):
        self.comments.extend(comment_ids)


class Entity(Collection):
    def __init__(self, name):
        Collection.__init__(self)
        self.name = name

    @classmethod
    @abc.abstractmethod
    def get_collection_name(cls):
        return 'Entity'


class Author(Entity):
    def __init__(self, author_name, article_ids: list):
        Entity.__init__(self, author_name)
        self.article_ids = article_ids or list()

    def add_article_to_author(self, article_id):
        self.article_ids.append(article_id)

    @classmethod
    def get_collection_name(cls):
        return 'Author'


class User(Entity):
    def __init__(self, author_name, comments: list):
        Entity.__init__(self, author_name)
        self.comments = comments or list()

    def add_comments_to_user(self, comments):
        self.comments.extend(comments)

    @classmethod
    def get_collection_name(cls):
        return 'User'


class Comment(Collection):
    def __init__(self, name, title, content, date, index):
        Collection.__init__(self)
        self.name = name
        self.subject = title
        self.content = content
        self.datetime = date
        self.id = index

    @classmethod
    def get_collection_name(cls):
        return 'Comment'


class RawScrapedData(Collection):
    def __init__(self, _id, url, raw_data, _cls):
        Collection.__init__(self)
        self._id = int(_id)
        self.article_id = _id
        self.url = url
        self.raw_data = raw_data
        self.article_cls = _cls

    def __hash__(self):
        return hash(self.article_id)

    @classmethod
    @abc.abstractmethod
    def get_collection_name(cls):
        return 'RawScrapedData'


class ScrapedInfoWarsData(RawScrapedData):
    def __init__(self, _id, url, raw_data):
        RawScrapedData.__init__(self, _id, url, raw_data, self.get_collection_name())

    @classmethod
    def get_collection_name(cls):
        return 'ScrapedInfoWarsData'


class ScrapedBuzzFeedData(RawScrapedData):
    def __init__(self, _id, url, raw_data):
        RawScrapedData.__init__(self, _id, url, raw_data, self.get_collection_name())

    @classmethod
    def get_collection_name(cls):
        return 'ScrapedBuzzFeedData'


class ParsedArticles(Collection):
    def __init__(self, article_id, keywords: list, title: str, quotes: list, paragraphs: list,
                 media_urls: list, network_urls: list):
        Collection.__init__(self)
        self.article_id = article_id
        self.keywords = keywords
        self.title = title
        self.quotes = quotes
        self.paragraphs = paragraphs
        self.media_urls = media_urls
        self.network_urls = network_urls

    @classmethod
    def get_collection_name(cls):
        return 'ParsedArticles'


class ParserJobs(Collection):
    def __init__(self, article_id, article_cls):
        Collection.__init__(self)
        self.article_id = article_id
        self.article_cls = article_cls
        self.parsed = False

    @classmethod
    def get_collection_name(cls):
        return 'ParserJobs'


class PopularKeywords(Collection):
    def __init__(self, keyword, count):
        Collection.__init__(self)
        self.keyword = keyword
        self.count = count

    @classmethod
    def get_collection_name(cls):
        return 'PopularKeywords'

