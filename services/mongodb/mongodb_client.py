import pymongo
import sys
from pymongo.database import Database
from pymongo.collection import Collection as MongoCollection
from services.mongodb.aps import AggregationPipeLineStages as Aps
from services.design_patterns.singleton import Singleton
from services.mongodb.collections import *
import time

PRODUCTION_CLUSTER = 'mongodb+srv://mamas:mamas@mamas0-xkchu.mongodb.net/test'


class MongoDBClient(metaclass=Singleton):
    def __init__(self, run_without_mongodb=False, config_string=PRODUCTION_CLUSTER):
        if config_string == PRODUCTION_CLUSTER:
            print('Connecting to MMS49 Prod')

        self.client: pymongo.MongoClient = None
        self.db: Database = None
        self.id = None
        self.idle: bool = run_without_mongodb
        self.config_string = config_string
        self._init_mongo()

    def _init_mongo(self):
        """
        Initializes the connection with the mongodb server.
        Run this function for the first time to connect to the server.
        """

        try:
            self.client = pymongo.MongoClient(self.config_string, serverSelectionTimeoutMS=1000)
            self.db = self.client['MMS49']
            collections = self.db.list_collection_names()
            if General.get_collection_name() not in collections:
                self._insert_data(General, {"type": "id_counter", "count": -2}, first=True)
            if Articles.get_collection_name() not in collections:
                self._insert_data(Articles, {}, first=True)
            if ScrapedInfoWarsData.get_collection_name() not in collections:
                self._insert_data(ScrapedInfoWarsData, {}, first=True)
            if ScrapedBuzzFeedData.get_collection_name() not in collections:
                self._insert_data(ScrapedBuzzFeedData, {}, first=True)

            self.update_id_counter()
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print("MongoDB is down!", file=sys.stderr)
            raise e
        # except Exception as e:
        #     print('Something has Happened: ', e)

        print("Mongodb initialized")

    def update_id_counter(self):
        """
        Sets the ID parameter and raises its value by 1

        :return: current id value
        """
        self.id = self._find_data(General, type="id_counter")[0]['count']
        self._update_data(General, "type", "id_counter", count=(self.id + 1))
        return self.id

    def _insert_data(self, collection, json, first=False, new_id=True):
        """
        Private function.
        Inserts the object json to the collection.
        Injects and id into it

        :param collection: The name of the collection that is to be inserted
        :param json: The object that is inserted into the DB
        :return:
        """
        try:
            col: MongoCollection = self.db[collection.get_collection_name()]

            if new_id:
                if 'id' in json.keys():
                    print('\'id\' parameter already exists!', file=sys.stderr)
                    raise Exception('\'id\' parameter already exists!')
                json['id'] = self.id

                if not first:
                    self.update_id_counter()

            res = col.insert_one(json)
            if first and collection not in [General.get_collection_name()]:
                col.delete_one({'_id': json['_id']})
            return res

        except pymongo.errors.DuplicateKeyError:
            search_id = json['_id']
            json.pop('_id')
            self._update_data(collection, '_id', search_id, **json)
            return search_id

        except Exception as e:
            print("Error raised while adding to %s: %s" % (collection, e), file=sys.stderr)
            raise e

    def insert_scraped_data(self, json):
        """
        Inserts the object `json` to the scraped data collection

        :param json: The object that is inserted
        :return: The id of the object in the DB
        """
        return self._insert_data(RawScrapedData, json)

    def insert_article(self, json):
        """
        Inserts the object `json` to the articles collection

        :param json: The object that is inserted
        :return: The id of the object in the DB
        """
        return self._insert_data(Articles, json)

    def _find_data(self, collection, as_gen=False, **kwargs):
        """
        Queries the database in collection `collection` to search for objects that have `key`: `value`

        :param collection: The name of the collection that is queried
        :param key: The key that is searched
        :param value: The value that is searched
        :return: A list of all values matching these parameters. Empty list means that no objects have been found.
        """
        try:
            col = self.db[collection.get_collection_name()]
            res = col.find({**kwargs})
            if as_gen:
                return res

            return list(res)
        except Exception as e:
            print("Error raised while querying to %s: %s" % (collection, e), file=sys.stderr)
            raise e

    def find_scraped_data(self, **kwargs):
        """
        Queries the raw_scraped_data collection for objects that have `key`: `value`

        :param key: The key that is searched
        :param value: The value that is searched
        :return: A list of all values matching these parameters. Empty list means that no objects have been found.
        """
        return self._find_data(RawScrapedData, **kwargs)

    def find_articles(self, limit=-1, page=-1, **kwargs):
        """
        Queries the articles collection for objects that have `key`: `value`
        Running this function without any parameters will return all of the articles
        """

        pipeline = list()
        pipeline.append(Aps.match_pipe(**kwargs))
        pipeline.append(Aps.lookup_pipe(Author.get_collection_name(), 'author', '_id', 'author'))
        pipeline.append(Aps.unwind_pipe('$author'))
        pipeline.append(Aps.sort_pipe(time_epoch=-1))
        pipeline.extend(Aps.facet_pipe(Aps.pagination_pipeline(page=page, limit=limit)))
        agg = self.aggregate_data(Articles, pipeline)

        if not agg.alive:
            return list()

        response = agg.next()
        return response['metadata']['total'], response.get('data', list())

    def get_article_full_data(self, article_id):
        try:
            total, _article = self.find_articles(id=article_id)
            article = _article[0]
        except IndexError:
            return dict()

        article_comments_ids = article['comments']
        comments = list(self.get_full_comment_data(article_comments_ids))
        article['comments'] = comments

        return article

    def _update_data(self, collection, search_key, search_value, **kwargs):
        """
        Updates collection `collection` - searches for items that match `search_key`: `search_value` and sets `set_key`: `set_value`

        :param collection: The collection that is searched
        :param search_key: The query search key
        :param search_value: The query search value
        :param set_key: The key of the value to be set
        :param set_value: The value to be set
        """
        try:
            col = self.db[collection.get_collection_name()]
            query = {search_key: search_value}
            values = {**kwargs}
            return col.update_one(query, {"$set": values})
        except Exception as e:
            print("Error raised while updating %s: %s" % (collection, e), file=sys.stderr)
            raise e

    def update_article(self, search_key, search_value, **kwargs):
        """
        Updates single article from Articles collection
        """
        return self._update_data(Articles, search_key, search_value, **kwargs)

    def insert_bulk(self, collection: Collection, documents: list, ordered=True):
        """
        insert and update
        :param collection:
        :param documents:
        :param ordered: for future when we will insert in bulks
        :return:
        """
        return [self._insert_data(collection, d, new_id=False) for d in documents]

    def aggregate_data(self, collection, pipeline):
        """
        data aggregation process to make sophisticated queries
        :param collection:
        :param pipeline:
        :return:
        """
        col = self.db[collection.get_collection_name()]
        res = col.aggregate(pipeline)
        if not res.alive:
            return list()

        return res

    def update_documents(self, collection: Collection, search_key, search_values, **kwargs):
        """
        updates fields in multiple documents
        :param collection:
        :param search_key:
        :param search_values:
        :param kwargs:
        :return:
        """
        for v in search_values:
            self._update_data(collection, search_key, v, **kwargs)

    def author_from_article(self, article: Articles):
        """
        get author object from article
        :param article:
        :return:
        """
        author_id = article.author
        if not author_id:
            return None
        res = self._find_data(Author, _id=ObjectId(author_id))
        if not res:
            return None

        return res[0]

    def get_full_comment_data(self, comment_ids: list):
        """
        get comments list from article
        :return:
        """

        if not comment_ids:
            return list()
        pipeline = list()

        pipeline.append(Aps.match_pipe(_id={'$in': comment_ids}))
        return self.aggregate_data(Comment, pipeline)

    def add_comment_to_article(self, comment_json, article_id):
        """
        inserts a comment to the Comment Collection, then updates te article with the comment id
        :param comment_json: 
        :param article_id: 
        :return: 
        """
        try:
            total, _article = self.find_articles(id=article_id)
            article: dict = _article[0]
        except IndexError:
            return None

        comment_json['datetime'] = time.strftime("%d/%m/%y %H:%M")

        comments = article.get('comments', list())

        comment_json['id'] = len(comments) + 1

        res = self._insert_data(Comment, comment_json, new_id=False)

        comments.append(res.inserted_id)

        return self.update_article('id', article_id, comments=comments)

    def get_title_from_parsed_articles(self, limit=-1):
        """
        gets the titles from all of the documents in the ParsedArticles collection
        :param limit: 
        :return: 
        """
        return (doc['title'] for doc in self.get_parsed_articles(limit, Aps.project_pipe(['title'])))

    def get_popular_keywords(self, limit=10):
        """
        gets popular keywords from the PopularKeywords collection
        :param limit: 
        :return: 
        """
        pipeline = list()
        if limit != -1:
            pipeline.append({'$limit': limit})
        pipeline.append({'$project': {
            '_id': -1, 'keyword': 1, 'count': 1
        }})
        return {d['keyword']: d['count'] for d in self.aggregate_data(PopularKeywords, pipeline)}

    def update_count_of_keyword(self, keyword, count):
        """
        updated frequency of a keyword in the database
        :param keyword: 
        :param count: 
        :return: 
        """
        self.db[PopularKeywords.get_collection_name()].update_one(
            dict(keyword=keyword), {'$inc': dict(count=count)}, upsert=True)

    def get_parsed_articles(self, limit=-1, project=None):
        """
        :return all of the document data from ParsedArticles collection
        :param limit: 
        :param project: 
        :return: 
        """
        pipeline = list()
        if limit != -1:
            pipeline.append({'$limit': limit})

        if project:
            pipeline.append(project)

        return self.aggregate_data(ParsedArticles, pipeline)
