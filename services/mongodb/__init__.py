import json
import sys
from os.path import exists as path_exists
from services.mongodb.mongodb_client import MongoDBClient

config_path = '%s/../config.json' % __path__[0]
config_dict = dict()
if path_exists(config_path):
    with open(config_path, 'r') as config:
        config_dict = json.loads(config.read())

mongo_db = None

# initializing mongodb
try:
    mongo_db = MongoDBClient(**config_dict)
except Exception as e:
    print('Failed to initialize mongodb:', e, file=sys.stderr)
    if not config_dict.get('run_without_mongodb', False):
        raise e
