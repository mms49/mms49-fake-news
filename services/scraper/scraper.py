from abc import abstractmethod, ABC
from services.mongodb.collections import ScrapedInfoWarsData, ScrapedBuzzFeedData
from bs4 import BeautifulSoup
from itertools import zip_longest
import requests
import time
import re

SPLIT_ID_WORD = "Context: "


class ArticleScraper(ABC):
    def __init__(self):
        self.raw_articles = dict()
        self.limit = None

    @abstractmethod
    def get_urls_to_scrape(self):
        pass

    @classmethod
    @abstractmethod
    def get_website_url(cls):
        pass

    @abstractmethod
    def get_scraped_article_cls(self):
        pass

    @abstractmethod
    def get_raw_data_from_websites(self, id_ranges=tuple()):
        pass

    @abstractmethod
    def scrape(self, callback=lambda *args, **kwargs: dict(), *cb_args, **cb_kwargs):
        pass


class InfoWarsScraper(ArticleScraper):
    def __init__(self, ranges=tuple()):
        ArticleScraper.__init__(self)
        self.article_id_key = '/?p='
        self.ranges = ranges

    def get_urls_to_scrape(self):
        info_wars_today = requests.get('https://www.%s' % self.get_website_url())

        if not info_wars_today.ok:
            raise Exception("Could not reach %s", self.get_website_url())

        return set(
            re.findall('https://www.%s' % '/'.join([self.get_website_url(), '[0-9A-Za-z-_]*']), info_wars_today.text))

    @classmethod
    def get_website_url(cls):
        return 'infowars.com'

    def get_scraped_article_cls(self):
        return 'InfoWarsArticles'

    def get_article_url(self, links):
        if 'shortlink' not in links:
            return None

        url: str = links['shortlink']['url']

        domain, article_id = url.split(self.article_id_key)

        return int(article_id)

    def get_raw_data_from_websites(self, id_ranges=tuple()):
        scraped_data = list()
        failed_on = list()

        article_urls = self.get_urls_to_scrape()

        _iterator = range(self.limit), article_urls
        iterator = zip_longest(*_iterator) if self.limit == -1 else zip(*_iterator)

        for n, url in iterator:
            result = requests.get(url)
            if not result.ok:
                failed_on.append(dict(request=url, error=result.content))
                continue

            article_id = self.get_article_url(result.links)

            if not article_id:
                failed_on.append(dict(request=url, error=result.content))
                continue

            scraped_data.append(ScrapedInfoWarsData(article_id, url, result.text))

        return dict(scraped_data=scraped_data, failed_on=failed_on)

    def scrape(self, callback=lambda *args, **kwargs: dict(), *cb_args, **cb_kwargs):
        jobs = callback(*cb_args, **cb_kwargs)
        if not jobs:
            raise Exception("Job is Empty")

        ranges = jobs['ranges']
        self.ranges = ranges
        self.limit = jobs['limit']

        return self.get_raw_data_from_websites()


class BuzzfeedScraper(ArticleScraper):
    buzzfeed_writers = [
        "benhenry",
        "emmalord9",
        "ikrd",
        "mjs538",
        "daves4",
        "laurengarafano",
        "hannahloewentheil",
        "melanie_aman",
        "nusrat21",
        "emmalord9",
        "sarahhan",
        "elliewoodward",
        "terrycarter",
        "ishabassi",
        "tatianatenreyrowhitlock",
        "farrahpenn",
        "tvandmovies",
        "bribri29",
        "mikespohr",
        "ajanibazile",
        "hannahalothman",
        "noradominick",
        "morganmurrell",
        "colingorenstein",
        "sarahhan",
        "hbraga",
        "laurengarafano",
        "morganmurrell",
        "jenniferabidor",
        "farrahpenn",
        "whitneyjefferson",
        "victoriagasparowicz",
        "andi28",
        "josieayre1",
        "samanthawieder",
        "eleanorbate",
        "kellymartinez",
        "izzvos11",
        "jamiespain",
        "jenniferabidor",
        "jonmichaelpoff",
        "kebornholtz",
        "elenamgarcia",
        "buzzfeedresearch",
        "yiyang",
        "daniellaemanuel"
    ]

    def __init__(self,):
        ArticleScraper.__init__(self)
        self.article_id_key = "/"

    @classmethod
    def re_find_article_id(cls, data):
        return re.search('"' + "id" + '"' + ":" + '"' + "[0-9a-zA-Z_-]*" + '"', data)

    @classmethod
    def get_id(cls, article_re):
        return int(article_re.group().split(":")[1].strip('"'))

    def get_urls_to_scrape(self):
        article_urls = list()
        for reporter in self.buzzfeed_writers:
            r = requests.get('https://%s' % '/'.join([self.get_website_url(), reporter]))
            if not r.ok:
                continue

            article_urls.extend(set(
                re.findall("https://www.%s" % '/'.join([self.get_website_url(), reporter, '[0-9A-Za-z-_]*']), r.text)))

        return article_urls

    @classmethod
    def get_website_url(cls):
        return 'buzzfeed.com'

    def get_scraped_article_cls(self):
        return 'BuzzFeedArticles'

    def get_raw_data_from_websites(self, id_ranges=tuple()):
        article_urls = self.get_urls_to_scrape()
        full_scrape_data = list()
        errors = list()

        _iterator = range(self.limit), article_urls
        iterator = zip_longest(*_iterator) if self.limit == -1 else zip(*_iterator)

        for n, article_urls in iterator:
            res = self.find_article_id_and_retrieve_raw_data(article_urls)
            if res:
                full_scrape_data.append(res)
            else:
                errors.append(article_urls)

        return dict(scraped_data=full_scrape_data, failed_on=errors)

    def find_article_id_and_retrieve_raw_data(self, article_url):
        content = requests.get(article_url)
        if not content.ok:
            return None

        soup = BeautifulSoup(content.text, features='html.parser')
        scripts = soup.find_all("script")

        context = [s.text for s in scripts if SPLIT_ID_WORD in s.text and 'window.BZFD' in s.text]

        if not len(context):
            return None

        article_re = self.re_find_article_id(context[0].split(SPLIT_ID_WORD)[1])

        if not article_re:
            return None

        article_id = self.get_id(article_re)
        print(article_id)
        return ScrapedBuzzFeedData(_id=article_id, url=article_url, raw_data=content.text)

    def scrape(self, callback=lambda *args, **kwargs: dict(), *cb_args, **cb_kwargs):
        jobs = callback(*cb_args, **cb_kwargs)
        if not jobs:
            raise Exception("Job is Empty")

        self.limit = jobs['limit']

        return self.get_raw_data_from_websites()
