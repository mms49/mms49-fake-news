from services.mongodb.collections import ParserJobs, ScrapedInfoWarsData, ScrapedBuzzFeedData
from services.scraper import InfoWarsScraper, BuzzfeedScraper
from services.mongodb import mongo_db


def callback():
    return dict(ranges=(718948, 718968), limit=50)


def run_scraper(scraper_type: str):
    if scraper_type == 'IW':
        return InfoWarsScraper().scrape(callback)
    elif scraper_type == 'BF':
        res = BuzzfeedScraper().scrape(callback)
        return res


if __name__ == '__main__':
    # platform = 'BF'
    platform = 'IW'
    raw_scraped_data_response = run_scraper(platform)
    raw_scraped_data_objects = raw_scraped_data_response['scraped_data']
    raw_failed_scraped_operations = raw_scraped_data_response['failed_on']
    collection = None

    if platform == 'IW':
        collection = ScrapedInfoWarsData
    elif platform == 'BF':
        collection = ScrapedBuzzFeedData
    else:
        raise Exception("No supported platform")

    mongo_db.insert_bulk(collection, [r.to_json() for r in raw_scraped_data_objects])

    mongo_db.insert_bulk(ParserJobs, map(lambda r: ParserJobs(int(r.article_id),
                                                              r.article_cls).to_json(), raw_scraped_data_objects))
