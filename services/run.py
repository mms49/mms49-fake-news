from services.flask_service import main
from services.scraper.main import run_scraper
from services.mongodb import mongo_db
import sys

# DO NOT RUN FROM HERE!
if __name__ == '__main__':

    if len(sys.argv) > 1:
        main_type = sys.argv[1]
    else:
        # main_type = 'app'
        main_type = 'scraper'

    if main_type == 'app':
        main.app.run(port=8153, debug=True)
    elif main_type == 'scraper':
        run_scraper()

