from services.mongodb import mongo_db


def should_use_mongodb(f):
    def inner(*args, **kwargs):
        if mongo_db.idle:
            return None
        return f(*args, **kwargs)
    return inner


def wait_on_empty_return(f):
    def inner(*args, **kwargs):
        wait = True
        while wait:
            res = f(*args, **kwargs)
            if not res:
                continue

            return res
    return inner
