from flask import Flask, render_template, send_file, request, redirect
from services.flask_service import template_dir
import sys
import os
from services.mongodb import mongo_db

sys.path.extend([os.path.abspath('../..')])
# if os.getcwd().replace('\\', '/').endswith("services/flask_service"):
#     template_dir = os.getcwd() + '/templates'
# else:
#     template_dir = os.getcwd()+'/services/flask_service/templates'

print("template_dir: " + str(template_dir))
print("os.listdir: " + str(os.path.isdir(template_dir)))
app = Flask(__name__, template_folder=template_dir)


# Yes. No. Maybe?
true = True
false = False


@app.route('/')
def index():
    limit = 5
    page = 1
    search = None
    try:
        limit = int(request.args.get('limit') or 5)
        page = int(request.args.get('page') or 1)
    except (ValueError,):
        render_template('errornotfound.html'), 404
    if request.args.get('Search-box') is None:
        total, articles = mongo_db.find_articles(page=page, limit=limit)
    else:
        query = {'$regex': '.*' + ''.join(str(x) + '.*' for x in str(request.args.get('Search-box')).split()), "$options": "-i"}
        articles_temp = [None] * 3
        total_temp = [None] * 3
        try:
            total_temp[0], articles_temp[0] = mongo_db.find_articles(title=query)
        except:
            total_temp[0], articles_temp[0] = 0, []
        try:
            total_temp[1], articles_temp[1] = mongo_db.find_articles(summary=query)
        except:
            total_temp[1], articles_temp[1] = 0, []
        try:
            total_temp[2], articles_temp[2] = mongo_db.find_articles(article=query)
        except:
            total_temp[2], articles_temp[2] = 0, []

        if sum(total_temp) == 0:
            return render_template('general.html', data='No results found')

        articles = []
        ids = []
        total = 0
        for result in articles_temp:
            for article in result:
                if article['id'] not in ids:
                    ids.append(article['id'])
                    articles.append(article)
                    total += 1
        articles = articles[(page - 1) * limit: page * limit]
        search = request.args.get('Search-box')
    count = total // limit + 1 if total % limit else total // limit
    if page > count:
        render_template('errornotfound.html'), 404
    return render_template('index.html', total=total, articles=articles, page=page, count=count, search=search)


@app.route('/post')  # /post?id=id
def post():
    try:
        id = int(request.args.get('id'))
    except ValueError:
        return render_template('errornotfound.html'), 404
    try:
        article = mongo_db.get_article_full_data(id)
    except:
        return render_template('errornotfound.html'), 404
    return render_template('post.html', article=article) if article else redirect("err")


@app.route('/<string:item>')
def general(item):
    if item == 'favicon.ico':
        return send_file('assets/img/favicon.ico', attachment_filename='favicon.ico')
        # return send_file('%s/assets/img/favicon.ico' % assets_dir, attachment_filename='favicon.ico')
    elif item == 'rickroll.mp4':
        return send_file('assets/videos/rickroll.mp4', attachment_filename='rickroll.mp4')
        # return send_file('%s/assets/videos/rickroll.mp4' % assets_dir, attachment_filename='rickroll.mp4')
    elif item == 'mamassymbolblack.png':
        return send_file('assets/img/mamassymbolblack.png', attachment_filename='mamassymbolblack.png')
    elif item == 'index.html':
        return redirect('/')
    else:
        return render_template('errornotfound.html'), 404


@app.route('/comment', methods=['POST'])
def comment():
    article_id = int(request.args.get('id'))

    c = dict(request.form)
    for i, v in c.items():
        if type(v) == list:
            c[i] = v[0]
        else:
            break

    res = mongo_db.add_comment_to_article(c, article_id)
    if not res:
        return redirect('err')

    return redirect('/post?id=%d' % article_id)


@app.route('/disclaimer')
def disclaimer():
    return render_template('disclaimer.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')

