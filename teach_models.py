from ai_news.generate_article import train_title_generator, train_article_generator
from ai_news.dataset.dataset_updater import create_datasets_from_scratch
import sys


def main():
    """
    Main function of script that creates articles.
    (designed to be called from commandline)
    :return:
    """
    arguments = sys.argv[1:]
    assert len(arguments) <= 2
    titles_epochs = None
    articles_epochs = None
    if len(arguments) > 0:
        titles_epochs = int(arguments[0])
        if len(arguments) > 1:
            articles_epochs = int(arguments[1])
    create_datasets_from_scratch(True)
    train_title_generator(epochs=titles_epochs)
    train_article_generator(epochs=articles_epochs)


if __name__ == '__main__':
    main()
